package com.gitlab.cromefire.detektgitlab.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Positions(
    /**
     * The position at which the code quality violation occurred.
     */
    val begin: Position,
) {
    @JsonClass(generateAdapter = true)
    data class Position(
        /**
         * Starts at 1.
         */
        val line: Int,
        /**
         * Starts at 1.
         */
        val column: Int,
    )
}
