plugins {
    kotlin("jvm") version "1.9.22" apply false
    id("io.gitlab.arturbosch.detekt") version "1.23.5" apply false
    id("com.github.johnrengelman.shadow") version "8.1.1" apply false
}
